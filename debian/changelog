libpdf-report-perl (1.36-2) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Nathan Handler from Uploaders. Thanks for your work!
  * Remove Ryan Niebur from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 16 Jun 2022 23:51:46 +0100

libpdf-report-perl (1.36-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 05 Jan 2021 16:33:37 +0100

libpdf-report-perl (1.36-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Nathan Handler ]
  * Email change: Nathan Handler -> nhandler@debian.org

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Florian Schlichting ]
  * Import Upstream version 1.36
  * Update upstream copyright holders, years and licenses
  * Split build-dependencies into B-D and B-D-I
  * Declare compliance with Debian Policy 3.9.4
  * Add headers to example-paths patch
  * Add myself to uploaders and copyright

 -- Florian Schlichting <fsfs@debian.org>  Thu, 03 Oct 2013 17:23:28 +0200

libpdf-report-perl (1.33-3) unstable; urgency=low

  * Team upload.

  [ Ryan Niebur ]
  * Update ryan52's email address

  [ Russ Allbery ]
  * Remove myself from Uploaders.
  * Upate debhelper compatibility level to V8.
    - Use debhelper rule minimization.
  * Avoid bashisms in debian/rules with an explicit list of file
    extensions whose permissions need fixing.
  * Change to Debian source format 3.0 (quilt).
    - Remove now-unnecessary README.source.
  * Remove version on perl build dependency satisfied by oldstable.
  * Use ${perl:Depends} for the perl package dependency instead of
    hard-coding a specific perl version.
  * Install upstream README file.
  * Rewrite debian/copyright in the new proposed DEP-5 format.
  * Remove version from libpdf-api2-perl build and package dependencies,
    satisfied by oldstable.
  * Merge Build-Depends and Build-Depends-Indep.  The separation is
    pointless for a source package that builds only arch-independent
    binary packages.
  * Move libtext-roman-perl to Recommends since it's only required for
    roman page numbering.  Remove version from dependency satisfied by
    oldstable.
  * Update standards version to 3.9.1 (no changes required).

 -- Russ Allbery <rra@debian.org>  Sun, 06 Mar 2011 16:56:02 -0800

libpdf-report-perl (1.33-2) unstable; urgency=low

  * SHELL=bash in debian/rules because theres a bashism (Closes:
    #535392)
  * Add myself to Uploaders

 -- Ryan Niebur <ryanryan52@gmail.com>  Thu, 02 Jul 2009 04:09:12 -0700

libpdf-report-perl (1.33-1) unstable; urgency=low

  [ Nathan Handler ]
  * New upstream release
  * debian/control:
    - Bump Standards-Version to 3.8.2 (No changes)
  * debian/patches/example-paths:
    - Update to apply

  [ gregor herrmann ]
  * debian/patches/example-paths: add new example script.

 -- Nathan Handler <nhandler@ubuntu.com>  Thu, 18 Jun 2009 04:47:27 +0000

libpdf-report-perl (1.32-1) unstable; urgency=low

  [ Nathan Handler ]
  * New upstream release

  [ gregor herrmann ]
  * debian/watch: update to ignore development releases.
  * debian/rules: remove restriction to run only some tests, since the faulty
    pod-coverage test is removed.
  * debian/copyright: add additional copyright holders for debian/*.

 -- Nathan Handler <nhandler@ubuntu.com>  Wed, 10 Jun 2009 02:07:26 +0000

libpdf-report-perl (1.31-1) unstable; urgency=low

  [ gregor herrmann ]
  * Add debian/README.source to document quilt usage, as required by
    Debian Policy since 3.8.0.
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).
  * debian/control: Added: ${misc:Depends} to Depends: field.

  [ Nathan Handler ]
  * New upstream release
  * debian/control:
    - Add myself to Uploaders list
    - Build-Depend on debhelper (>= 7)
    - Bump Build-Depends-Indep and Depends on libpdf-api2-perl to >= 0.40.00
    - Bump Depends on libtext-roman-perl to >= 3.02
    - Bump Standards-Version to 3.8.1
  * debian/libpdf-report-perl.examples:
    - Move examples here from debian/rules
  * debian/copyright:
    - Modify to use new format
  * debian/rules:
    - Update to use standard rules format for Perl modules
    - Make images in examples directory non-executable
  * debian/docs
    - Remove, not needed
  * debian/patches/fix-addImgScaled:
    - Remove, applied upstream
  * debian/patches/uninitialized-warnings:
    - Remove, applied upstream
  * debian/patches/pod-fixes:
    - Remove, applied upstream
  * debian/patches/scale-barcodes:
    - Remove, applied upstream
  * debian/patches/example-paths:
    - Update for modified example files
  * debian/patches/series:
    - Remove fix-addImgScaled
    - Remove uninitialized-warnings
    - Remove pod-fixes
    - Remove scale-barcodes
  * debian/compat:
    - Bump to 7

  [ gregor herrmann ]
  * debian/watch: use our "default" regexp for matching upstream versions.
  * debian/control:
    - change my email address
    - add build dependency on libtest-pod-perl (used in test suite)
  * debian/rules: run only specific tests.

 -- Nathan Handler <nhandler@ubuntu.com>  Sun, 07 Jun 2009 02:25:39 +0000

libpdf-report-perl (1.30-7) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed: XS-
    Vcs-Svn fields.
  * Use dist-based URL in debian/watch.
  * Set debhelper compatibility level to 5.
  * debian/rules: delete /usr/lib/perl5 only if it exists.
    (Closes: #467843)

  [ Russ Allbery ]
  * Switch to quilt from dpatch.
  * debian/rules cleanup:
    - Use $(CURDIR) instead of `pwd`.
    - Add build-arch and build-indep targets.
    - Add an install-stamp file and touch $@ instead of the stamp name.
    - Reorganize and reformat.
    - Remove unnecessary dh_installdirs call and thereby eliminate an
      empty /usr/share/perl5/PDF/Report directory.
  * debian/control: Wrap Uploaders and Depends.
  * debian/copyright: Add an explicit copyright statement and reformt.
  * Update standards version to 3.7.3 (no changes required).

 -- Russ Allbery <rra@debian.org>  Mon, 03 Mar 2008 22:05:15 -0800

libpdf-report-perl (1.30-6) unstable; urgency=low

  [ gregor herrmann ]
  * Added patch by David Pottage to fix addImgScaled(), thanks!
    (Closes: #365471)

  [ Russ Allbery ]
  * Update to standards version 3.7.0 (no changes required).
  * Remove the empty /usr/lib/perl5 directory from the binary package.
  * Remove template comments from debian/rules.

 -- Russ Allbery <rra@debian.org>  Sun, 30 Apr 2006 20:21:07 -0700

libpdf-report-perl (1.30-5) unstable; urgency=low

  * debian/rules: don't compress images in examples directory.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Tue, 25 Apr 2006 01:13:02 +0200

libpdf-report-perl (1.30-4) unstable; urgency=low

  * Migrate to dpatch.
  * debian/patches/40warnings:
    + fix several 'Use of uninitialized value' warnings. (Closes: #354679)
  * Move debhelper to Build-Depends, since it's required for 'clean'.
  * debian/patches/30examples:
    + fix the perl and module path in 'examples/image' as well.

 -- Niko Tyni <ntyni@iki.fi>  Fri, 17 Mar 2006 22:27:50 +0200

libpdf-report-perl (1.30-3) unstable; urgency=low

  * Scale barcodes correctly.  Thanks, jim@short.circuit.com.
    (Closes: #343937)
  * Fix some POD errors in the PDF::Report documentation.
  * Remove commented-out templates in debian/rules per ftp-master.
  * Remove obsolete source and diff rules.
  * Use debian/compat rather than DH_COMPAT per debhelper recommendation.

 -- Russ Allbery <rra@debian.org>  Mon, 16 Jan 2006 14:30:26 -0800

libpdf-report-perl (1.30-2) unstable; urgency=low

  [ Niko Tyni ]
  * Fixed maintainer email address.

  [ Gunnar Wolf ]
  * debian/watch was including modules in the same namespace, fixed.
  * Now the clean target in debian/rules does not blindly go through the
    realclean call
  * Bumped up standards-version to 3.6.2

 -- Gunnar Wolf <gwolf@debian.org>  Tue, 13 Dec 2005 21:58:13 -0600

libpdf-report-perl (1.30-1) unstable; urgency=low

  * New upstream release

 -- Gunnar Wolf <gwolf@debian.org>  Thu, 17 Mar 2005 18:49:54 -0600

libpdf-report-perl (1.22-2) unstable; urgency=low

  * Fixed usage of private (and deprecated) PDF::API2 attributes which
    broke the whole thing. Applied patch by Jordan Hrycaj - Thanks!
    (Closes: #292622)

 -- Gunnar Wolf <gwolf@debian.org>  Tue, 15 Feb 2005 20:36:48 -0600

libpdf-report-perl (1.22-1) unstable; urgency=low

  * New upstream version - Thanks to Allard Hoeve <allard@byte.nl> for
    prompting for this upload
  * Transferred maintenance to the Debian Perl Group <pkg-perl-
    maintainer@lists.alioth.debian.org>

 -- Gunnar Wolf <gwolf@debian.org>  Thu, 27 Jan 2005 09:55:36 -0600

libpdf-report-perl (1.20-2) unstable; urgency=low

  * Fixed a missing build-dependency on libpdf-api2-perl (Closes:
    #223176)
  * Bumped up standards-version to 3.6.1

 -- Gunnar Wolf <gwolf@debian.org>  Mon,  8 Dec 2003 16:13:02 -0600

libpdf-report-perl (1.20-1) unstable; urgency=low

  * New upstream release
  * Added the upstream-provided examples to the documentation
  * Switched build method to debhelper

 -- Gunnar Wolf <gwolf@debian.org>  Tue, 25 Nov 2003 15:38:32 -0600

libpdf-report-perl (1.10-2) unstable; urgency=low

  * Section change (interpreters -> perl)

 -- Gunnar Wolf <gwolf@debian.org>  Thu, 19 Jun 2003 13:43:12 -0500

libpdf-report-perl (1.10-1) unstable; urgency=low

  * New upstream release

 -- Gunnar Wolf <gwolf@debian.org>  Fri, 25 Apr 2003 11:30:26 -0500

libpdf-report-perl (1.00-1) unstable; urgency=low

  * Initial Release.

 -- Gunnar Eyal Wolf Iszaevich <gwolf@gwolf.cx>  Mon, 10 Mar 2003 17:49:07 -0600
